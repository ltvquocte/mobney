package ltvqt.com.mobney.fragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ltvqt.com.mobney.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CalendarPopupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalendarPopupFragment extends DialogFragment {

    public CalendarPopupFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CalendarPopupFragment.
     */
    public static CalendarPopupFragment newInstance() {
        CalendarPopupFragment fragment = new CalendarPopupFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        getDialog().setTitle(R.string.title_fragment_select_period);

        return inflater.inflate(R.layout.fragment_calendar_popup, container, false);
    }
}
