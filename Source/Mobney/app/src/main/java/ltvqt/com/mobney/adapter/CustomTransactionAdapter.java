package ltvqt.com.mobney.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import ltvqt.com.mobney.R;
import ltvqt.com.mobney.model.TransactionModel;
import ltvqt.com.mobney.utils.AndroidUtils;

/**
 * Created by hoaile on 5/21/2016.
 */
public class CustomTransactionAdapter extends ArrayAdapter<TransactionModel> {

    Activity context;
    List<TransactionModel> data;
    int resource;

    public CustomTransactionAdapter(Activity context, int resource, List<TransactionModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.data = objects;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        convertView = inflater.inflate(resource, null);

        TransactionModel transaction = data.get(position);

        TextView tvDate = (TextView) convertView.findViewById(R.id.tv_transaction_list_date);

        tvDate.setText(transaction.getDate());

        ListView lvDetails = (ListView) convertView.findViewById(R.id.list_view_transaction_detail);

        // set height
        float height = transaction.getDetails().size() * 50 + (transaction.getDetails().size() - 1) * 10; // height 50dp and padding 10dp
        height = AndroidUtils.dpToPx(context, height);

        lvDetails.getLayoutParams().height = (int) height;

        CustomTransactionDetailAdapter adapter = new CustomTransactionDetailAdapter(
                context,
                R.layout.transaction_list_detail_item,
                transaction.getDetails());

        lvDetails.setAdapter(adapter);

        return convertView;
    }
}
