package ltvqt.com.mobney.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import ltvqt.com.mobney.utils.AndroidUtils;

/**
 * Created by hoaile on 5/18/2016.
 */
public class PieChart extends View {

    private PieChartData[] dataSource;

    public void setDataSource(PieChartData[] dataSource) {

        this.dataSource = dataSource;
        this.invalidate();
    }

    final String[] colorArr = new String[]{"#2196f3", "#4caf50", "#f44336", "#ff9800"};

    public PieChart(Context context) {

        super(context);
    }

    public PieChart(Context context, AttributeSet attrs) {

        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);

        if (dataSource != null && dataSource.length > 0) {

            final float chartBorder = AndroidUtils.dpToPx(getContext(), 35);

            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL_AND_STROKE);

            Paint paintText = new Paint();
            paintText.setColor(Color.BLACK);
            paintText.setTextSize(AndroidUtils.dpToPx(getContext(), 14));

            int left = 0;
            int height = getHeight();
            int top = 0;
            int startAngle = -90;

            RectF rectF = new RectF();
            rectF.set(left, top, left + height, top + height);

            float descriptionDistance = height / (dataSource.length * 2 - 1);
            float startY = 0;
            final float marginDescription = AndroidUtils.dpToPx(getContext(), 20);

            for (int i = 0; i < dataSource.length; i++) {

                paint.setColor(Color.parseColor(colorArr[i]));
                canvas.drawArc(rectF, startAngle, dataSource[i].getAngle(), true, paint);
                startAngle += dataSource[i].getAngle();

                // draw description
                canvas.drawRect(height + marginDescription, startY,
                        height + marginDescription + AndroidUtils.dpToPx(getContext(), 10),
                        startY + AndroidUtils.dpToPx(getContext(), 10), paint);

                canvas.drawText(dataSource[i].getLabel(), height + marginDescription + AndroidUtils.dpToPx(getContext(), 15),
                        startY + descriptionDistance / 2, paintText);

                startY += descriptionDistance;
            }


            RectF rectCenter = new RectF();

            final float opacityWidth = AndroidUtils.dpToPx(getContext(), 5);

            paint.setColor(Color.parseColor("#30ffffff"));

            rectCenter.set(left + chartBorder - opacityWidth,
                    top + chartBorder - opacityWidth,
                    left + height - chartBorder + opacityWidth,
                    top + height - chartBorder + opacityWidth);

            canvas.drawArc(rectCenter, 0, 360, true, paint);

            paint.setColor(Color.WHITE);

            rectCenter.set(left + chartBorder, top + chartBorder, left + height - chartBorder, top + height - chartBorder);
            canvas.drawArc(rectCenter, 0, 360, true, paint);
        }
    }

    public static class PieChartData {

        public PieChartData() {
        }

        private float angle;
        private String label;


        public float getAngle() {
            return angle;
        }

        public void setAngle(float angle) {
            this.angle = angle;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }
    }
}
