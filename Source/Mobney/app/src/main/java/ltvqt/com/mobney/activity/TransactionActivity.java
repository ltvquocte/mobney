package ltvqt.com.mobney.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.Window;

import ltvqt.com.mobney.R;

public class TransactionActivity extends AppCompatActivity {

    public static final String BUNDLE_PAID_TAG = "is_paid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Bundle b = getIntent().getExtras();
        boolean isPaid = b.getBoolean(BUNDLE_PAID_TAG);

        if (!isPaid) {
            setTheme(R.style.AppTheme_TransactionPage_UnPaid);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);

        Window window = getWindow();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.account_menu, menu);
        return true;
    }
}
