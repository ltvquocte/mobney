package ltvqt.com.mobney.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import ltvqt.com.mobney.utils.AndroidUtils;
import ltvqt.com.mobney.utils.CalculateUtils;

/**
 * Created by hoaile on 5/20/2016.
 */
public class LineChart extends View {

    private float[] dataSource;

    public LineChart(Context context) {

        super(context);
    }

    public LineChart(Context context, AttributeSet attrs) {

        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);

        if (dataSource != null || dataSource.length != 0) {

            float maxY = CalculateUtils.getMax(dataSource);

            // draw axes X, Y
            final int width = getWidth();
            final int height = getHeight();

            final float axeWidth = 2;
            final float circleWith = AndroidUtils.dpToPx(getContext(), 2);

            final float labelWidth = AndroidUtils.dpToPx(getContext(), 50);

            Paint axePaint = new Paint();
            axePaint.setStrokeWidth(axeWidth);
            axePaint.setTextSize(36);

//        canvas.drawLine(0, 0, 0, height, axePaint);
//        canvas.drawLine(0, height, width, height, axePaint);

            // draw Y axist
            final int range = CalculateUtils.getRange(maxY);

            float startY = 0;
            while (startY < maxY) {

                float y = height - (startY / maxY) * height - axeWidth;

                axePaint.setColor(Color.parseColor("#cccccc"));
                canvas.drawLine(labelWidth, y, width, y, axePaint);

                axePaint.setColor(Color.GRAY);
                canvas.drawText(String.valueOf(startY), 0, y, axePaint);
                startY += range;
            }

            // draw line chart

            float startX = labelWidth;
            float distanceX = (width - axeWidth - circleWith - labelWidth) / (dataSource.length - 1);

            Paint chartPaint = new Paint();
            chartPaint.setStrokeWidth(AndroidUtils.dpToPx(getContext(), 1));
            chartPaint.setColor(Color.parseColor("#f44336"));

            for (int i = 0; i < dataSource.length - 1; i++) {

                float y1 = height - (dataSource[i] / maxY) * (height - axeWidth) - axeWidth + circleWith;
                float x1 = startX + axeWidth;

                if (i == 0) {

                    canvas.drawCircle(x1, y1, circleWith, chartPaint);
                }

                startX += distanceX;

                float y2 = height - (dataSource[i + 1] / maxY) * (height - axeWidth) - axeWidth + circleWith;
                float x2 = startX + axeWidth;

                canvas.drawLine(x1, y1, x2, y2, chartPaint);
                canvas.drawCircle(x2, y2, circleWith, chartPaint);
            }
        }
    }

    public void setDataSource(float[] dataSource) {
        this.dataSource = dataSource;
        this.invalidate();
    }
}
